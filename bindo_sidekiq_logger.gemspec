# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bindo_sidekiq_logger/version'

Gem::Specification.new do |spec|
  spec.name          = "bindo_sidekiq_logger"
  spec.version       = BindoSidekiqLogger::VERSION
  spec.authors       = ["zhu bo"]
  spec.email         = ["bo.zhu@bindolabs.com"]

  spec.summary       = %q{bindo sidekiq logger, to log performance}
  spec.description   = %q{bindo sidekiq logger, to log performance}
  spec.homepage      = "https://bitbucket.org/zhu_bo/bindo_sidekiq_logger"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://bitbucket.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry", ">= 0.10"
  spec.add_development_dependency "rb-readline"
  spec.add_runtime_dependency 'multi_json'
  spec.add_runtime_dependency 'activesupport', "~> 3.2"
  spec.add_runtime_dependency "sidekiq", "~> 2.17.2"
end
