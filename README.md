# BindoSidekiqLogger

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/bindo_sidekiq_logger`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bindo_sidekiq_logger'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install bindo_sidekiq_logger

## Usage

```ruby
class SampleJob
  include Sidekiq::Worker
  sidekiq_options queue: "test_queue", retry: false
  def perform(arg1, arg2 = {}, arg3)
    (1..100000).to_a.reduce(&:+)
  end
end
# specify your logger you like here
@logger = Logger.new('./log/application.log')
# in app initialize
BindoSidekiqLogger.config do |config|
  # assign your logger here
  # if does not specify a config.loger, it will create a logger file in ./log/application.log. need this to log errors if any.
  # config.logger = @logger
  # add an existing Sidekiq Worker to blacklist
  # config.add_to_blacklist("JobInBlacklist")
  # run server side middleware, default true
  # config.server_enabled = true
  # run client side middleware, default true
  # config.client_enabled = true
end

BindoSidekiqLogger.init

# consume it
ActiveSupport::Notifications.subscribe(/bindo_sidekiq_logger.*/) do |name, start, finish, id, payload={}|
  payload.merge!({
    job_name:   name,
    started_at: start,
    finish_at:  finish,
    duration:   finish.to_i - start.to_i,
    id: id
  })
  # or you can just ignore the logger specified in the app. Just use your own logger here. for example: Rails.logger
  BindoSidekiqLogger.logger.info payload.to_json
end

```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.


## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/bindo_sidekiq_logger.

