require 'active_support/all'
require 'sidekiq'
require 'logger'
require "bindo_sidekiq_logger/version"
require "bindo_sidekiq_logger/config"
require "bindo_sidekiq_logger/middleware/client_wrapper"
require "bindo_sidekiq_logger/middleware/server_wrapper"

module BindoSidekiqLogger
  def self.config
    @config ||= BindoSidekiqLogger::Config.new
    yield @config if block_given?
    @config
  end

  def self.init
    return self if @config.initialized
    self.add_server_middleware(BindoSidekiqLogger::ServerWrapper) if @config.server_enabled
    self.add_client_middleware(BindoSidekiqLogger::ClientWrapper) if @config.client_enabled
    self.register_middlewares_to_sidekiq
    @config.initialized = true
  end

  def self.add_client_middleware(middleware_name)
    self.config.add_client_middleware(middleware_name)
  end

  def self.add_server_middleware(middleware_name)
    self.config.add_server_middleware(middleware_name)
  end

  def self.register_middlewares_to_sidekiq
    if self.config.server_middlewares.any?
      Sidekiq.configure_server do |config|
        config.server_middleware do |chain|
          self.config.server_middlewares.each do |middleware|
            chain.add middleware unless chain.exists? middleware
          end
        end
      end
    end

    if self.config.client_middlewares.any?
      Sidekiq.configure_client do |config|
        config.client_middleware do |chain|
          self.config.client_middlewares.each do |middleware|
            chain.add middleware unless chain.exists? middleware
          end
        end
      end
    end
  end

  def self.logger
    self.config.logger
  end
  def self.env
    ENV['RACK_ENV'] || ENV['RAILS_ENV']
  end

  def self.production?
    self.env == "production"
  end
end
