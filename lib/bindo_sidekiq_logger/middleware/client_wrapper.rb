module BindoSidekiqLogger
  class ClientWrapper
    attr_reader :blacklist
    def initialize
      @blacklist = BindoSidekiqLogger.config.blacklist
    end

    def logger(job_name='')
      BindoSidekiqLogger.config.logger
    end

    def call(worker_class, job, queue, redis_pool = nil)
      if @blacklist[:client].include?(worker_class.to_s)
        # do nothing if the job in blacklist
        yield
      else
        payload = {
          retry: job["retry"],
          queue: job["queue"],
          class: job["class"],
          args:  job["args"],
          jid:   job["jid"],
          enqueued_at: Time.at(job["enqueued_at"]),
          from: "Client",
          status: "success"
        }

        ActiveSupport::Notifications.instrument "bindo_sidekiq_logger.#{worker_class.to_s}", payload do
          begin
            yield
          rescue => e
            payload[:status] = 'failed'
            failed_at = Time.now
            payload[:message]   = "[#{failed_at}] JobFailed: #{item.class.to_s}, With args #{worker['args']}."
            payload[:failed_at] = failed_at
            logger.error [e.message, payload.to_json].concat(e.backtrace[0..5])
            raise
          end
        end
      end
    end # end of call
  end # end of wrapper
end # end of BindoSidekiqLogger

