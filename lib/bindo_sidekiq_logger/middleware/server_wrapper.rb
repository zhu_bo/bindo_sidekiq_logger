module BindoSidekiqLogger
  class ServerWrapper
    include Sidekiq::Util
    attr_reader :blacklist
    def logger
      BindoSidekiqLogger.config.logger
    end

    def initialize
      @blacklist = BindoSidekiqLogger.config.blacklist
    end

    def call(worker, item, queue)
      if @blacklist[:server].include?(item.class.to_s)
        yield
      else
        payload = {
          args:   worker["args"],
          queue:  queue,
          class:  item.class.name,
          from:   "Server",
          status: 'success'
        }

        ActiveSupport::Notifications.instrument "bindo_sidekiq_logger.#{item.class.to_s}", payload do
          begin
            yield
          rescue => e
            payload[:status] = 'failed'
            failed_at = Time.now
            payload[:message]   = "[#{failed_at}] JobFailed: #{item.class.to_s}, With args #{worker['args']}."
            payload[:failed_at] = failed_at
            logger.error [e.message, payload.to_json].concat(e.backtrace[0..5])
            raise e
          end
        end
      end
    end # end of call
  end # end of wrapper
end # end of BindoSidekiqLogger