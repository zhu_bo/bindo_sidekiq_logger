require 'fileutils'
module BindoSidekiqLogger
  class Config
    attr_accessor :logger,
                  :blacklist,
                  :client_middlewares,
                  :server_middlewares,
                  :subscribe_enabled,
                  :server_enabled,
                  :client_enabled,
                  :initialized,
                  :default_logger_path
    def initialize
      @blacklist = {
        server: [],
        client: []
      }
      @customized_jobs    = []
      @client_middlewares = []
      @server_middlewares = []
      @subscribe_enabled  = true
      @server_enabled     = true
      @client_enabled     = true
      @initialized        = false
      @default_logger_path= './log/application.log'
    end

    def add_to_blacklist(job_name, options = { server: true, client: true })
      if options[:server]
        @blacklist[:server].concat([*job_name])
        @blacklist[:server].uniq!
      end

      if options[:client]
        @blacklist[:client].concat([*job_name])
        @blacklist[:client].uniq!
      end

      @blacklist
    end

    def register_queue(queue_name)
      @registered_queue.concat([*queue_name])
    end

    def add_client_middleware(middleware_class)
      @client_middlewares.concat([*middleware_class])
      @client_middlewares.uniq!
    end

    def logger
      @logger ||= Logger.new(@default_log_path)
    end

    def add_server_middleware(middleware_class)
      @server_middlewares.concat([*middleware_class])
      @server_middlewares.uniq!
    end
  end
end