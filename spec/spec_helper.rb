$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)

require "bindo_sidekiq_logger"
require "sidekiq"
require 'sidekiq/testing'
require 'pry'
RSpec.configure do |config|
  config.before(:each) do |example_method|
    Sidekiq::Testing.fake!
  end
end
