require "spec_helper"
require 'logger'
require 'json'

describe BindoSidekiqLogger, sidekiq: :inline do
  before :all do
    class SampleJob
      include Sidekiq::Worker
      sidekiq_options queue: "test_queue", retry: false
      def perform(arg1, arg2 = {}, arg3)
        (1..100000).to_a.reduce(&:+)
      end
    end

    class SampleJobToLogfile
      include Sidekiq::Worker
      sidekiq_options queue: "test_queue", retry: false
      def perform(arg1, arg2 = {}, arg3)
        sleep(1)
      end
    end

    class JobWillRaiseError
      include Sidekiq::Worker
      sidekiq_options queue: "test_queue", retry: false
      def perform(arg1, arg2 = {}, arg3)
        1/0
      end
    end

    class JobInBlacklist
      include Sidekiq::Worker
      sidekiq_options queue: "test_queue", retry: false

      def perform(arg1)
      end
    end

    @logger = Logger.new('./log/application.log')

    BindoSidekiqLogger.config do |config|
      config.logger = @logger
    end

    BindoSidekiqLogger.init

    ActiveSupport::Notifications.subscribe(/bindo_sidekiq_logger.*/) do |name, start, finish, id, payload={}|
      payload.merge!({
        job_name:   name,
        started_at: start,
        finish_at:  finish,
        duration:   finish.to_i - start.to_i,
        id: id
      })
      BindoSidekiqLogger.config.logger.info payload.to_json
    end
  end

  it "has a version number" do
    expect(BindoSidekiqLogger::VERSION).not_to be nil
  end

  context "With sample Job" do
    it "does something useful" do
      @logger.should_receive(:info).with(/SampleJob.*success/).once
      SampleJob.perform_async(1, {a: 1, b:2}, "arg3")
    end

    it "serverside middleware" do
      @logger.should_receive(:info).with(/SampleJobToLogfile|success/).once

      msg = {"args" => [1, {:a=>1, :b=>2}, "arg3"]}
      worker = SampleJobToLogfile.new

      BindoSidekiqLogger::ServerWrapper.new.call(msg, worker, "test_queue") do
        worker.perform(*msg['args'])
      end
    end

    it "will capture error" do
      @logger.should_receive(:error).with(array_including(/JobWillRaiseError.*failed/))
      @logger.should_receive(:info).with(/JobWillRaiseError|failed/)
      worker = JobWillRaiseError.new
      msg    = {"args" => [1, {:a=>1, :b=>2}, "arg3"]}
      expect do
        BindoSidekiqLogger::ServerWrapper.new.call(msg, worker, "test_queue") do
          worker.perform(*msg['args'])
        end
      end.to raise_error ZeroDivisionError
    end

    it "will ignore job in blacklist" do
      @logger.should_not_receive(:info)
      @logger.should_not_receive(:error)

      BindoSidekiqLogger.config do |config|
        config.add_to_blacklist("JobInBlacklist")
      end

      JobInBlacklist.perform_async("blacklist_job")
    end
  end
end
